package ru.bokov;

/**
 * Created by Дмитрий on 1.12.2015.
 */
public class Demo extends Thread {

    public int count;
    public synchronized void increment(String name){
        count++;
        System.out.println(name+count);
    }

    public void run() {
        for (int j = 0; j < 50; j++) {
            increment(getName());
        }
    }

}
